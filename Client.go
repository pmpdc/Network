package Network

import (
	"net"
	"time"
)

type Client struct {
	ctx *Context
}

func NewClient(handler func() (net.Conn, error)) (*Client, error) {
	c := &Client{ctx: &Context{}}
	conn, err := handler()
	if err != nil {
		return nil, err
	}
	c.ctx.Conn = conn
	return c, nil
}

func (c *Client) Call(endpoint string, payload []byte, response *[]byte, timeoutAfter time.Duration) error {
	message := NewMessage(endpoint, payload)

	err := message.WriteTo(c.ctx.Conn)
	if err != nil {
		return err
	}

	responseMessage, err := NewMessageFromConnection(c.ctx.Conn)
	if err != nil {
		return err
	}

	*response = responseMessage.payload
	return nil
}

func (c *Client) CloseConnection() error {
	return c.ctx.Conn.Close()
}
