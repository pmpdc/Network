package Network

import (
	"net"
)

type Context struct {
	Conn     net.Conn
	sequence int
	Message  *Message
}

func (c *Context) Write(payload []byte) error {
	message := NewMessage("", payload)
	err := message.WriteTo(c.Conn)
	return err
}
