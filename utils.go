package Network

import (
	"encoding/binary"
	"io"
	"net"
)

const (
	BodyLenSize = 4 // bytes
	PathLenSize = 1 // bytes

	payloadLenSizeStartsAt = 0
	payloadLenSizeEndsAt   = BodyLenSize

	pathLenSizeStartsAt = BodyLenSize
	pathLenSizeEndsAt   = BodyLenSize + PathLenSize

	payloadStartsAtByte = BodyLenSize + PathLenSize
)

type Message struct {
	path    string
	payload []byte
}

func NewMessageFromConnection(conn net.Conn) (*Message, error) {
	var headerBytes = [BodyLenSize + PathLenSize]byte{}
	if _, err := io.ReadFull(conn, headerBytes[:]); err != nil {
		return nil, err
	}
	pathLen := headerBytes[pathLenSizeStartsAt:pathLenSizeEndsAt][0] // its is just 1 byte
	payloadLen := binary.LittleEndian.Uint32(headerBytes[payloadLenSizeStartsAt:payloadLenSizeEndsAt])

	pathBytes := make([]byte, pathLen)
	if _, err := io.ReadFull(conn, pathBytes); err != nil {
		return nil, err
	}

	payloadBytes := make([]byte, payloadLen)
	if _, err := io.ReadFull(conn, payloadBytes); err != nil {
		return nil, err
	}

	message := Message{
		path:    string(pathBytes),
		payload: payloadBytes,
	}

	return &message, nil
}

func NewMessage(path string, payload []byte) *Message {
	return &Message{
		path:    path,
		payload: payload,
	}
}

func (c *Message) Payload() []byte {
	return c.payload
}

func (c *Message) WriteTo(buffer io.Writer) error {
	payloadSize := make([]byte, BodyLenSize)
	binary.LittleEndian.PutUint32(payloadSize, uint32(len(c.payload)))

	//buffer := bytes.NewBuffer(payloadSize) // write payload header
	if _, err := buffer.Write(payloadSize); err != nil {
		return err
	}

	// ser path len
	if _, err := buffer.Write([]byte{byte(len(c.path))}); err != nil {
		return err
	}

	// set path
	if _, err := buffer.Write([]byte(c.path)); err != nil {
		return err
	}

	// payload
	if _, err := buffer.Write(c.payload); err != nil {
		return err
	}

	return nil
}
