package Network

import (
	"fmt"
	"log"
	"net"
)

type Router struct {
	Listener net.Listener
	Routes   map[string]func(conn *Context)
}

func NewRouter() *Router {
	r := Router{
		Routes: map[string]func(conn *Context){},
	}
	return &r
}

func (r *Router) Add(path string, handler func(conn *Context)) {
	r.Routes[path] = handler
}

func (r *Router) handleConnection(conn net.Conn) {
	message, err := NewMessageFromConnection(conn)

	if err != nil {
		println("handleConn err")
		println(err.Error())
		conn.Close()
		return
	}

	handler, exists := r.Routes[message.path]

	if exists == false {
		println(fmt.Sprintf("path %s, is not registered", message.path))
		conn.Close()
		return
	}
	handler(&Context{
		Conn:     conn,
		sequence: 0,
		Message:  message,
	})
}

func (r *Router) Run(listener net.Listener) {
	r.Listener = listener
	for {
		conn, err := r.Listener.Accept()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Calling handleConnection")
		go r.handleConnection(conn)
	}
}
